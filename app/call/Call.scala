package call

import java.time.{Instant, OffsetDateTime, ZoneId}

import contact.Contact
import number.PhoneNumber
import play.api.libs.json._
import reactivemongo.bson.{BSONDocument, BSONDocumentHandler, BSONString, Macros}
import uk.co.unclealex.mongodb._


/**
  * The main model for received calls.
  * @param when The instant when the call was received.
  * @param caller The contact or number who made the call.
  */
case class Call(_id: Option[ID], when: Instant, caller: Caller, hidden: Option[Boolean] = Some(false)) {

  /**
    * A more friendly view of this call
    */
  val view: CallView = {
    val (maybeContact, maybePhoneNumber): (Option[Contact], Option[PhoneNumber]) = caller.view
    CallView(_id.map(_.id).getOrElse(""), when, maybeContact, maybePhoneNumber, hidden.getOrElse(false))
  }
}

/**
  * The model for who has made a call. This can be either [[Withheld]] if the number was withheld,
  * [[Known]] if the number is from a contact and [[Unknown]] if it is not.
  */
sealed trait Caller {
  val view: (Option[Contact], Option[PhoneNumber])
}

/**
  * The caller instance for when the number is withheld.
  */
object Withheld extends Caller {

  override val view: (Option[Contact], Option[PhoneNumber]) = (None, None)
}

/**
  * The caller instance for when a call is from a contact.
  * @param name The name of the contact.
  * @param phoneType The type of the phone.
  * @param avatarUrl A URL to an avatar for the contact, if any.
  * @param phoneNumber The number that called.
  */
case class Known(name: String, phoneType: String, avatarUrl: Option[String], phoneNumber: PhoneNumber)
  extends Caller {

  override val view: (Option[Contact], Option[PhoneNumber]) =
    (Some(Contact(phoneNumber.normalisedNumber, name, phoneType, avatarUrl)), None)
}

/**
  * The caller instance for when a call is not from a contact.
  * @param phoneNumber The number that called.
  */
case class Unknown(phoneNumber: PhoneNumber) extends Caller {
  override val view: (Option[Contact], Option[PhoneNumber]) = (None, Some(phoneNumber))
}

/**
  * A [[CallView]] is a view of a [[Call]] that is better for front ends. Both the contact and phone number are
  * optional and so this means that html templates, for example, do not have to do case matching.
  * @param when The instant the call was made.
  * @param contact The contact who called, if known.
  * @param phoneNumber The phone number that called, if known.
  */
case class CallView(
                     id: String,
                     when: Instant,
                     contact: Option[Contact],
                     phoneNumber: Option[PhoneNumber],
                     hidden: Boolean) {

  def whenWithTimezone(zoneId: ZoneId): OffsetDateTime = OffsetDateTime.ofInstant(when, zoneId)
}

/**
  * Json codecs for [[Caller]]. Basically, the subclass is determined by a discriminator property known as `type`
  */
object Caller {

  private val withheldHandler: BSONDocumentHandler[Caller] = BSONDocumentHandler[Caller](
    write = _ => BSONDocument.empty,
    read = _ => Withheld
  )

  private val knownHandler: BSONDocumentHandler[Known] = Macros.handler[Known]
  private val unknownHandler: BSONDocumentHandler[Unknown] = Macros.handler[Unknown]

  implicit val callerHandler: BSONDocumentHandler[Caller] = BSONDocumentHandler[Caller](
    write = caller => {
      val (obj, ty) = caller match {
        case w@Withheld => (withheldHandler.write(w), "withheld")
        case k: Known => (knownHandler.write(k), "known")
        case u: Unknown => (unknownHandler.write(u), "unknown")
      }
      obj ++ ("type" -> BSONString(ty))

    },
    read = bson => {
      bson.getAs[String]("type") match {
        case Some("withheld") => withheldHandler.read(bson)
        case Some("known") => knownHandler.read(bson)
        case Some("unknown") => unknownHandler.read(bson)
        case _ => throw new UnsupportedOperationException("caller.discriminator")
      }

    }
  )

  implicit val callerFormat: OFormat[Caller] = {
    val withheldFormat: OFormat[Caller] = new OFormat[Caller] {
      override def reads(json: JsValue): JsResult[Caller] = JsSuccess(Withheld)

      override def writes(o: Caller): JsObject = JsObject.empty
    }
    val knownFormat: OFormat[Known] = Json.format[Known]
    val unknownFormat: OFormat[Unknown] = Json.format[Unknown]
    new OFormat[Caller] {
      override def reads(json: JsValue): JsResult[Caller] = {
        val objResult: JsResult[JsObject] = json match {
          case obj: JsObject => JsSuccess(obj)
          case _ => JsError("An object is required to read a caller")
        }
        for {
          obj <- objResult
          ty <- (obj \ "type").validate[String]
          caller <- {
            ty match {
              case "withheld" => withheldFormat.reads(obj)
              case "known" => knownFormat.reads(obj)
              case "unknown" => unknownFormat.reads(obj)
            }
          }
        } yield {
          caller
        }
      }

      override def writes(caller: Caller): JsObject = {
        val (obj, ty) = caller match {
          case w@Withheld => (withheldFormat.writes(w), "withheld")
          case k: Known => (knownFormat.writes(k), "known")
          case u: Unknown => (unknownFormat.writes(u), "unknown")
        }
        obj + ("type" -> JsString(ty))
      }
    }
  }
}

/**
  * Json codecs for [[Call]]
  */
object Call {

  implicit val callIsPersistable: IsPersistable[Call] = IsPersistable(
    getId = _._id,
    setId = id => _.copy(_id = Some(id))
  )

  implicit val callHandler: BSONDocumentHandler[Call] = Macros.handler[Call]

  implicit val callFormat: OFormat[Call] = Json.format[Call]
}


/**
  * Json codecs for [[CallView]]
  */
object CallView {
  implicit val callViewWrites: OWrites[CallView] = Json.writes[CallView]
}