package call

import java.time.{Clock, Instant}

import cats.data.Validated.{Invalid, Valid}
import com.typesafe.scalalogging.StrictLogging
import contact.{Contact, ContactDao, PersistedContact}
import modem.{Ignore, ModemResponse}
import number.PhoneNumberFactory

import scala.concurrent.{ExecutionContext, Future}

/**
  * The default implementation of [[CallService]]
  * @param clock
  * @param phoneNumberFactory
  * @param contactService
  * @param ec
  */
class CallServiceImpl(
                       clock: Clock,
                       phoneNumberFactory: PhoneNumberFactory,
                       contactService: ContactDao)
                     (implicit ec: ExecutionContext) extends CallService with StrictLogging {

  override def call(modemResponse: ModemResponse): Future[Option[Either[String, Call]]] = {
    logger.info(s"Modem: $modemResponse")
    val now: Instant = clock.instant()
    def call(caller: Caller): Option[Either[String, Call]] = Some(Right(Call(None, now, caller)))
    val none: Future[Option[Either[String, Call]]] = Future.successful(None)
    def error(line: String): Future[Option[Either[String, Call]]] = Future.successful(Some(Left(line)))
    modemResponse match {
      case modem.Withheld => Future.successful(call(Withheld))
      case modem.Unknown(line) => error(line)
      case modem.Number(number) => phoneNumberFactory(number) match {
        case Valid(phoneNumber) =>
          contactService.findContactForPhoneNumber(phoneNumber.normalisedNumber).map {
            /*
                                         _id: Option[ID],
                             userEmailAddress: String,
                             normalisedPhoneNumber: String,
                             name: String,
                             phoneType: String,
                             avatarUrl: Option[String],
                             lastUpdated: Option[Instant],
                             dateCreated: Option[Instant])

             */
            case Some(persistedContact) => call(
              Known(
                persistedContact.name,
                persistedContact.phoneType,
                persistedContact.avatarUrl,
                phoneNumber)
            )
            case None => call(Unknown(phoneNumber))
          }
        case Invalid(_) => none
      }
      case Ignore(_) => none
    }
  }
}