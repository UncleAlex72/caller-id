package call

import java.time.{Clock, Instant}

import contact.Contact
import reactivemongo.api.DefaultDB
import reactivemongo.bson.BSONString
import uk.co.unclealex.mongodb._

import scala.concurrent.{ExecutionContext, Future}

/**
  * An implementation of [[CallDao]] that uses MongoDB
  * @param reactiveMongoApi The underlying MongoDB api.
  * @param ec The execution context for futures.
  */
class MongoDbCallDao(override val databaseProvider: () => Future[DefaultDB], override val clock: Clock)(implicit ec: ExecutionContext) extends
  MongoDbDao[Call](databaseProvider, clock, "calls") with CallDao {

  override def insert(call: Call): Future[Unit] = {
    store(call).map(_ => {})
  }


  override def alterHidden(id: ID, hidden: Boolean): Future[Option[Call]] = {
    update("_id" === id, call => call.copy(hidden = Some(hidden)))
  }

  override def calls(max: Option[Int], since: Option[Instant], until: Option[Instant]): Future[Seq[Call]] = {
    val maxDocs: Int = max.getOrElse(Int.MaxValue)
    findWhere("when" ?>=&<= (since, until), max = maxDocs, sort = "when".desc)
  }

  override def alterContacts(contacts: Seq[Contact]): Future[Int] = {
    val updateInstructions: Seq[UpdateInstruction] = contacts.map { contact =>
      val query: Query =
        "caller.type" === "unknown" && "caller.phoneNumber.normalisedNumber" === contact.normalisedPhoneNumber
      val nonAvatarUpdate: Update = $set(
        "caller.name" -> BSONString(contact.name),
        "caller.type" -> BSONString("known"),
        "caller.phoneType" -> BSONString(contact.phoneType))
      val update: Update = contact.avatarUrl.foldLeft(nonAvatarUpdate) { (update, avatarUrl) =>
        update && $set("caller.avatarUrl" -> BSONString(avatarUrl))
      }
      UpdateInstruction(query, update, upsert = false, multi = true)
    }
    updateWhere(updateInstructions)
  }
}
