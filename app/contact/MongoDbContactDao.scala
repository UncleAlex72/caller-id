package contact
import java.time.{Clock, Instant}

import cats.implicits._
import reactivemongo.api.DefaultDB
import reactivemongo.bson.{BSONDocumentHandler, Macros}
import uk.co.unclealex.mongodb.Bson._
import uk.co.unclealex.mongodb._

import scala.concurrent.{ExecutionContext, Future}
import scala.language.implicitConversions

/**
  * An implementation of [[ContactDao]] that uses MongoDB
  * @param reactiveMongoApi The underlying mongo API.
  * @param executionContext The execution context for futures.
  */
class MongoDbContactDao(override val databaseProvider: () => Future[DefaultDB], clock: Clock)(implicit executionContext: ExecutionContext) extends
  MongoDbDao[PersistedContact](databaseProvider, clock, "contacts") with ContactDao {

  override def upsertUser(user: User): Future[Unit] = {
    for {
      _ <- deleteUser(user.emailAddress)
      _ <- insertUser(user)
    } yield {}
  }

  def deleteUser(emailAddress: String): Future[Int] = {
    remove("userEmailAddress" === emailAddress)
  }

  def insertUser(user: User): Future[Unit] = {
    val empty: Future[Unit] = Future.successful({})
    user.contacts.foldLeft(empty) {(acc, contact) =>
      val persistedContact = PersistedContact(user.emailAddress, contact)
      for {
        _ <- acc
        _ <- store(persistedContact)
      } yield {}
    }
  }

  override def findContactForPhoneNumber(normalisedPhoneNumber: String): Future[Option[PersistedContact]] = {
    for {
      contacts <- collection()
      cursor = contacts.find("normalisedPhoneNumber" === normalisedPhoneNumber).cursor[PersistedContact]()
      maybeContact <- cursor.headOption
    } yield {
      maybeContact
    }
  }

  override def findContactByName(name: String): Future[Option[PersistedContact]] = {
    findOne("name" === name)
  }
}

/**
  * The persisted, non-normalised, form of a contact.
  * @param userEmailAddress The email address of the user associated with the contact.
  * @param normalisedPhoneNumber The contact's normalised phone number.
  * @param name The name of the contact.
  * @param phoneType The phone type.
  * @param avatarUrl The contact's avatar URL, if they have one.
  */
case class PersistedContact(
                             _id: Option[ID],
                             userEmailAddress: String,
                             normalisedPhoneNumber: String,
                             name: String,
                             phoneType: String,
                             avatarUrl: Option[String],
                             lastUpdated: Option[Instant],
                             dateCreated: Option[Instant])

object PersistedContact {
  import uk.co.unclealex.mongodb.Bson._

  def apply(emailAddress: String, contact: Contact): PersistedContact = {
    PersistedContact(
      None,
      emailAddress,
      contact.normalisedPhoneNumber,
      contact.name,
      contact.phoneType,
      contact.avatarUrl,
      None,
      None)
  }
  implicit val persistedContactIsPersistable: IsPersistable[PersistedContact] = IsPersistable(
    getId = _._id,
    setId = id => _.copy(_id = Some(id)),
    getDateCreated = _.dateCreated,
    setDateCreated = instant => _.copy(dateCreated = Some(instant)),
    getLastUpdated = _.lastUpdated,
    setLastUpdated = instant => _.copy(lastUpdated = Some(instant)),
  )

  implicit val persistedContactHandler: BSONDocumentHandler[PersistedContact] = Macros.handler[PersistedContact]
}
