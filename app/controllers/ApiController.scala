package controllers

import call.{Call => _, _}
import com.mohiva.play.silhouette.api.actions.SecuredActionBuilder
import contact.{ContactDao, ContactLoader}
import number.{NumberFormatter, PhoneNumberFactory}
import play.api.mvc.{Filters => _, _}
import uk.co.unclealex.psm.{DefaultEnv, Silhouette, SilhouetteAuthorization, SilhouetteDao, User}
import uk.co.unclealex.psm.User._
import uk.co.unclealex.push.BrowserPushService

import scala.concurrent.{ExecutionContext, Future}
import play.api.libs.json._
import uk.co.unclealex.mongodb.ID
import uk.co.unclealex.futures.Futures._

/**
  * The main controller used for interacting with browsers.
  * @param numberLocationService
  * @param numberFormatter
  * @param callDao
  * @param contactLoader
  * @param contactService
  * @param browserPushService
  * @param silhouette
  * @param authorization
  * @param authInfoDao
  * @param assets
  * @param controllerComponents
  * @param ec
  */
class ApiController(
                      val numberLocationService: PhoneNumberFactory,
                      val numberFormatter: NumberFormatter,
                      val callDao: CallDao,
                      val contactLoader: ContactLoader,
                      val contactService: ContactDao,
                      val silhouette: Silhouette,
                      val authorization: SilhouetteAuthorization,
                      val authInfoDao: SilhouetteDao,
                      val browserPushService: BrowserPushService,
                      val assets: Assets,
                      override val controllerComponents: ControllerComponents)(implicit val ec: ExecutionContext)
  extends AbstractController(controllerComponents) {

  val secure: SecuredActionBuilder[DefaultEnv, AnyContent] = silhouette.SecuredAjaxAction(authorization)

  def userInfo: Action[AnyContent] = secure { implicit request =>
    Ok(Json.toJson(request.identity))
  }

  def calls: Action[AnyContent] = secure.async { implicit request =>
    callDao.calls(max = Some(20)).map { calls =>
      Ok(Json.toJson(calls.map(_.view)))
    }
  }

  def hidden(id: ID): Action[Boolean] = secure.async(parse.json[Boolean]) { implicit request =>
    val hidden: Boolean = request.body
    fo(callDao.alterHidden(id, hidden)).map { call =>
      Ok(Json.toJson(call.view))
    }.getOrElse(NotFound)
  }

  /**
    * Update a user's contacts.
    * @return
    */
  def updateContacts: Action[AnyContent] = secure.async { implicit request =>
    def failWith[V](maybeValue: Option[V], errorMessage: String): Future[V] = {
      maybeValue.map(Future.successful).getOrElse(Future.failed(new IllegalStateException(errorMessage)))
    }
    val user: User = request.identity
    for {
      emailAddress <- failWith(user.email, "email address required")
      maybeAccessToken <- authInfoDao.find(user.loginInfo)
      accessToken <- failWith(maybeAccessToken, "access token required")
      user <- contactLoader.loadContacts(emailAddress, accessToken.accessToken)
      _ <- contactService.upsertUser(user)
      _ <- callDao.alterContacts(user.contacts)
    } yield {
      Created(JsObject.empty)
    }
  }
}