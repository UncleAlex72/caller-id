package controllers

import call.{Call => _}
import cats.data._
import contact.ContactDao
import play.api.libs.ws.WSClient
import play.api.mvc.{Filters => _, _}
import uk.co.unclealex.futures.Futures._
import uk.co.unclealex.psm.{Silhouette, SilhouetteAuthorization}

import scala.concurrent.{ExecutionContext, Future}

/**
  * Resolve avatar URLs.
  * @param silhouette
  * @param authorization
  * @param controllerComponents
  * @param ec
  */
class AvatarController(
                      val contactDao: ContactDao,
                      val silhouette: Silhouette,
                      val authorization: SilhouetteAuthorization,
                      val wsClient: WSClient,
                      override val controllerComponents: ControllerComponents)(implicit val ec: ExecutionContext)
  extends AbstractController(controllerComponents) {

  def avatar(name: String): Action[AnyContent] = silhouette.SecuredAction(authorization).async { implicit request =>
    val meResult: FutureOption[Result] = for {
      persistedContact <- fo(contactDao.findContactByName(name))
      avatarUrl <- fo(persistedContact.avatarUrl)
      response <- fo(wsClient.url(avatarUrl).get())
    } yield {
      val contentType: String = response.contentType
      Ok.chunked(response.bodyAsSource).as(contentType)
    }
    meResult.getOrElse(NotFound)
  }

}

