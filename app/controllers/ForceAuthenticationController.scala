package controllers

import call.{Call => _, _}
import contact.{ContactDao, ContactLoader}
import number.{NumberFormatter, PhoneNumberFactory}
import play.api.libs.json._
import play.api.mvc.{Filters => _, _}
import uk.co.unclealex.psm.User._
import uk.co.unclealex.psm.{Silhouette, SilhouetteAuthorization, SilhouetteDao, User}
import uk.co.unclealex.push.BrowserPushService

import scala.concurrent.{ExecutionContext, Future}

/**
  * A controller used to force authentication whilst in development mode as front-end resources are served up
  * as JSON via a javascript proxy and so there is no chance for the SPA router to force authentication.
  */
class ForceAuthenticationController(
                      val silhouette: Silhouette,
                      val authorization: SilhouetteAuthorization,
                      override val controllerComponents: ControllerComponents)
  extends AbstractController(controllerComponents) {

  def index(): Action[AnyContent] = silhouette.SecuredAction(authorization) { implicit request =>
    Ok("<html><head/><body><p>Authorised</p></body></html>").as("text/html")
  }
}

