package dialogflow

import java.time.format.DateTimeFormatter
import java.time.{OffsetDateTime, ZoneId}

import call.{Call, CallView}
import misc.NonEmptyListExtensions._
import number.CountrySummary

/**
  * The default implementation of [[CallToSpeechService]]
  * @param dateTimeFormatter A formatter used to format the date and time the call was made.
  * @param zoneId The Zone ID of the required time zone.
  */
class CallToSpeechServiceImpl(dateTimeFormatter: DateTimeFormatter, zoneId: ZoneId) extends CallToSpeechService {
  override def speak(call: Call): String = {
    val callView: CallView = call.view
    val when: String = dateTimeFormatter.format(OffsetDateTime.ofInstant(callView.when, zoneId))
    val caller: String = callView.contact match {
      case Some(contact) => s"${contact.name} called"
      case None =>
        callView.phoneNumber match {
          case Some(phoneNumber) =>
            val location: String = phoneNumber.city match {
              case Some(city) => {
                val country: String = formatCountry(phoneNumber.countries.head)
                s"$city, $country"
              }
              case None => phoneNumber.countries.map(formatCountry).join(", ", " or ")
            }
            s"There was a call from ${phoneNumber.formattedNumber} in $location"
          case None => "There was a withheld call"
        }
    }
    s"$caller on $when"
  }

  def formatCountry(nameOrSummary: Either[String, CountrySummary]): String = {
    nameOrSummary match {
      case Left(name) => name
      case Right(summary) => summary.name
    }
  }
}
