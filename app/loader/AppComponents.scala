package loader

import java.time.{Clock, ZoneId}

import akka.actor.ActorSystem
import auth._
import call._
import cats.data.NonEmptyList
import com.typesafe.scalalogging.StrictLogging
import contact.{ContactDao, GoogleContactLoader, MongoDbContactDao}
import controllers._
import datetime.{DaySuffixes, DaySuffixesImpl}
import dialogflow._
import notify.Notifier
import notify.sinks._
import loader.ConfigLoaders._
import mocks.{LoggingMediaServerBuilder, ModemSender, SendableAtModem}
import modem.{Modem, TcpAtModem}
import number._
import play.api.ApplicationLoader
import play.api.ConfigLoader._
import play.api.cache.ehcache.EhCacheComponents
import play.api.libs.ws.ahc.AhcWSComponents
import play.api.mvc.{Call => PlayCall}
import play.api.routing.Router
import router.Routes
import routers.SpaRouter
import uk.co.unclealex.psm.{PlaySilhouetteMongoFromContext, SilhouetteAuthorization}
import uk.co.unclealex.push.PushComponents
import uk.co.unclealex.squeezebox.MediaServer.MediaServer
import uk.co.unclealex.squeezebox.{AkkaStreamMediaServerClient, MediaServer, SqueezeboxPing}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

/**
  * Create all the components needed to run this application.
  * @param context The Play Context.
  */
class AppComponents(context: ApplicationLoader.Context)
  extends PlaySilhouetteMongoFromContext(context, "caller-id")
    with AhcWSComponents
    with AssetsComponents
    with EhCacheComponents
    with PushComponents
    with StrictLogging {

  def redirectOnLogin: PlayCall = PlayCall("GET", "/caller-id/index.html")
  def redirectOnLogout: PlayCall = PlayCall("GET", "/caller-id/index.html")

  override def ec: ExecutionContext = executionContext

  val cityDao: CityDao = CityDaoImpl()

  val mongoExecutionContext: ExecutionContext = actorSystem.dispatcher

  val locationConfiguration: LocationConfiguration = {
    val internationalDiallingCodes: Set[String] = cityDao.all().map(_.internationalDiallingCode).toSet
    val countryCode: String = configuration.getAndValidate[String]("location.countryCode", internationalDiallingCodes)
    val country: Country = cityDao.countries(countryCode).get.head
    val stdCodes: Set[String] = country.cities.map(_.stdCode).toSet
    val stdCode: String = configuration.getAndValidate[String]("location.stdCode", stdCodes)
    logger.info(s"Creating location configuration with international dialling code $countryCode and STD code $stdCode")
    val zoneId: ZoneId = configuration.getOptional[ZoneId]("location.timezone").getOrElse(ZoneId.systemDefault())
    LocationConfiguration(countryCode, stdCode, zoneId)
  }
  val daySuffixes: DaySuffixes = DaySuffixesImpl

  val localService: LocalService = new LocalServiceImpl(locationConfiguration.internationalCode, locationConfiguration.stdCode)
  val numberFormatter: NumberFormatter = new NumberFormatterImpl(localService)
  val numberLocationService: PhoneNumberFactory = new PhoneNumberFactoryImpl(cityDao, numberFormatter, localService)

  val clock: Clock = Clock.systemDefaultZone()
  val contactDao: ContactDao = new MongoDbContactDao(databaseProvider, clock)(mongoExecutionContext)
  val callService: CallService = new CallServiceImpl(clock, numberLocationService, contactDao)

  val callDao: CallDao = new MongoDbCallDao(databaseProvider, clock)(mongoExecutionContext)
  val (modem: Modem, maybeModemSender: Option[ModemSender]) = {
    val modemConfiguration: ModemConfiguration = configuration.get[ModemConfiguration]("modem")
    modemConfiguration match {
      case DebugModemConfiguration =>
        val modem = new SendableAtModem()(actorSystem, materializer)
        (modem, Some(modem))
      case NetworkModemConfiguration(host, port) =>
        (new TcpAtModem(host, port)(actorSystem, materializer), None)
    }
  }

  val validUsers: Seq[String] = configuration.get[String]("silhouette.emails").split(',').map(_.trim)
  val authorization: SilhouetteAuthorization = new ValidUserAuthorization(validUsers)
  val contactLoader = new GoogleContactLoader(numberLocationService)
  val apiController = new ApiController(
    numberLocationService,
    numberFormatter,
    callDao,
    contactLoader,
    contactDao,
    silhouette,
    authorization,
    authInfoDao,
    browserPushService,
    assets,
    controllerComponents)

  val debugModemController = new DebugModemController(maybeModemSender, controllerComponents)
  val callToSpeechService = new CallToSpeechServiceImpl(new WebhookResponseDateTimeFormatter(daySuffixes)(), locationConfiguration.zoneId)
  val queryParameterService = new QueryParameterServiceImpl(clock)
  val dialogflowService = new DialogflowServiceImpl(queryParameterService,
    callToSpeechService,
    callDao)
  val dialogflowToken: String = configuration.get[String]("dialogflow.token")
  val dialogflowController = new DialogflowController(dialogflowService, dialogflowToken, controllerComponents)

  val notifier: Notifier = {
    implicit val _ac: ActorSystem = actorSystem
    val persistingSink = new PersistingSink(callDao)
    val pushNotificationSink = new PushNotificationSink(browserPushService)
    val squeezeboxConfiguration: SqueezeboxConfiguration = configuration.get[SqueezeboxConfiguration]("squeezebox")
    val squeezeboxSink: CallSink = {
      val mediaServerBuilder: () => MediaServer = squeezeboxConfiguration match {
        case DebugSqueezeboxConfiguration => LoggingMediaServerBuilder()
        case NetworkSqueezeboxConfiguration(host, port) => {
          val mediaServer = MediaServer(host, port)
          registerHealthCheck("squeezebox", SqueezeboxPing(mediaServer), 1.minute)
          mediaServer
        }
      }
      val mediaServerClient = new AkkaStreamMediaServerClient(mediaServerBuilder)
      new SqueezeboxSink(mediaServerClient, configuration.get[FiniteDuration]("squeezebox.duration"))
    }

    val notifier = new Notifier(
      modem,
      callService,
      NonEmptyList.of(
        persistingSink,
        pushNotificationSink,
        squeezeboxSink
      ))

    val modemHealthCheck: () => Future[String] = () => {
      Future.successful(notifier.checkConnection()).map(_ => "ok")
    }
    registerHealthCheck("modem", modemHealthCheck, 1.minute)

    applicationLifecycle.addStopHook { () => Future.successful(notifier.disconnect()) }
    notifier
  }

  val avatarController = new AvatarController(contactDao, silhouette, authorization, wsClient, controllerComponents)

  override def router: Router = new Routes(
    httpErrorHandler,
    apiController,
    avatarController,
    pushController,
    dialogflowController,
    socialAuthController,
    new ForceAuthenticationController(silhouette, authorization, controllerComponents),
    debugModemController,
    secureMetricsController,
    healthCheckController,
    new SpaRouter(assets)
  )
}
