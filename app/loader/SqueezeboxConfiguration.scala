package loader

/**
  * A model used to configure the squeezebox.
  */
sealed trait SqueezeboxConfiguration

/**
  * Indicate that the "debug" squeezebox is to be used.
  */
object DebugSqueezeboxConfiguration extends SqueezeboxConfiguration

/**
  * Configuration for the squeezebox server configuration.
  * @param host The squeezebox host.
  * @param port The squeezebox port.
  */
case class NetworkSqueezeboxConfiguration(host: String, port: Int) extends SqueezeboxConfiguration