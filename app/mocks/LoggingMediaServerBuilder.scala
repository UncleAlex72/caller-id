package mocks

import java.io

import com.typesafe.scalalogging.StrictLogging
import uk.co.unclealex.squeezebox.MediaServer
import uk.co.unclealex.squeezebox.models
import uk.co.unclealex.squeezebox.MediaServer.MediaServer
import uk.co.unclealex.squeezebox.models.Player
import uk.co.unclealex.squeezebox.{SqueezeboxRequest => Req}
import uk.co.unclealex.squeezebox.{SqueezeboxResponse => Res}

object LoggingMediaServerBuilder extends StrictLogging {

  private val playersById: Map[String, models.Player] = Map(
    "0" -> models.Player("0", "Upstairs", connected = true),
    "1" -> models.Player("1", "Downstairs", connected = true)
  )
  private val players: Seq[Player] = playersById.values.toSeq

  def apply(): () => MediaServer = MediaServer.mock {
    case Req.Players =>
      logger.info(s"Returning ${players.size} players.")
      Res.Players(players)
    case Req.DisplayMessage(id, topLine, bottomLine, duration) => {
      val playerName: String = playersById.get(id).map(_.name).getOrElse(s"Unknown $id")
      logger.info(s"$playerName: [$topLine, $bottomLine] for $duration")
      Res.DisplayMessage(models.DisplayMessage(id, topLine, bottomLine, duration))
    }
  }
}
