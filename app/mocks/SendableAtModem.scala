package mocks

import akka.actor.ActorSystem
import akka.stream.scaladsl.{Flow, Sink, Source}
import akka.stream.{Materializer, OverflowStrategy}
import akka.util.ByteString
import akka.{Done, NotUsed}
import com.typesafe.scalalogging.StrictLogging
import modem.AtModem

import scala.concurrent.Future

/**
  * A fake modem that actually takes its commands from strings that are sent to it.
  * @param actorSystem
  * @param materializer
  */
class SendableAtModem(implicit actorSystem: ActorSystem, materializer: Materializer)
  extends AtModem with ModemSender with StrictLogging {

  val sink: Sink[ByteString, Future[Done]] = Sink.foreach[ByteString] { request =>
    logger.info(s"Received ${request.utf8String}")
  }
  val (queue, source) = Source.queue[ByteString](0, OverflowStrategy.backpressure).preMaterialize()
  val flow: Flow[ByteString, ByteString, NotUsed] = Flow.fromSinkAndSource(sink, source)

  override def createConnection(): Flow[ByteString, ByteString, _] = flow

  override def send(line: String): Unit = {
    val trimmedLine: String = line.trim
    logger.info(s"Sending to modem: $trimmedLine")
    queue.offer(ByteString(s"$trimmedLine\n"))
  }
}
