package modem

/**
 * A trait describing the output of a modem.
 * Created by alex on 31/08/15.
 */
trait Modem {

  /**
   *
   * @return A source of modem responses as they are received from the modem.
   */
  def responses(): ModemResponseSource
}
