package modem

/**
 * An abstract of all the valid modem responses.
 */
sealed trait ModemResponse

/**
 * A response that should be ignored.
 */
case class Ignore(line: String) extends ModemResponse

/**
 * The response when a withheld number calls.
 */
case object Withheld extends ModemResponse

/**
 * The response when a line that cannot be understood is returned from the modem.
 */
case class Unknown(line: String) extends ModemResponse

/**
 * The response when a non-withheld number calls.
 */
case class Number(
                   /**
                    * The number sent via the modem.
                    */
                   number: String) extends ModemResponse
