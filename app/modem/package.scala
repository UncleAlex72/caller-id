import akka.stream.FlowMonitor
import akka.stream.scaladsl.Source

package object modem {

  // Type aliases to make working with modem streams less verbose.

  type ModemSource[A] = Source[A, (Disconnect, FlowMonitor[_])]
  type ModemResponseSource = ModemSource[ModemResponse]

  /**
    * A class that can be used to disconnect fromm the modem.
    */
  trait Disconnect {

    /**
      * Disconnect from a modem.
      */
    def disconnect()
  }

}
