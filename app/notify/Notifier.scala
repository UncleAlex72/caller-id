package notify

import _root_.notify.sinks.CallSink
import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.FlowMonitorState.{Failed, Finished}
import akka.stream.Materializer
import akka.stream.scaladsl._
import call.{Call, CallService}
import cats.data.NonEmptyList
import com.typesafe.scalalogging.StrictLogging
import modem.{Disconnect, Modem, ModemResponseSource, ModemSource}

import scala.concurrent.ExecutionContext

/**
  * The class that listens for modem response events and pushes them to a list of sinks.
  * @param modem       The [[Modem]] to listen to.
  * @param callService The [[CallService]] used to turn [[modem.ModemResponse]]s into [[Call]]s.
  * @param sinkActions The list of sink actions to send calls to.
  * @param actorSystem
  * @param materializer
  * @param ec
  */
class Notifier(val modem: Modem,
               callService: CallService,
               sinkActions: NonEmptyList[CallSink])
              (implicit val actorSystem: ActorSystem, materializer: Materializer, ec: ExecutionContext) extends StrictLogging {

  logger.info("Starting modem notifications")

  val callsSource: ModemSource[Either[String, Call]] =
    modem.responses().
      mapAsync(16)(r => callService.call(r)).
      flatMapConcat {
        case Some(callOrError) => Source.single(callOrError)
        case None => Source.empty
      }

  val sinks: NonEmptyList[Sink[Either[String, Call], NotUsed]] =
    sinkActions.map { originalAction =>
      val newAction: Either[String, Call] => Unit = {
        case Left(error) => originalAction.consumeError(error)
        case Right(call) => originalAction.consumeCall(call)
      }
      newAction
    }.map(action => Sink.foreachParallel(16)(action).mapMaterializedValue(_ => NotUsed))

  val sink: Sink[Either[String, Call], NotUsed] = {
    val firstSink: Sink[Either[String, Call], NotUsed] = sinks.head
    sinks.tail match {
      case Nil => firstSink
      case secondSink :: otherSinks => Sink.combine(firstSink, secondSink, otherSinks :_*)(Broadcast(_))
    }
  }

  val (_disconnect, _flowMonitor) = callsSource.to(sink).run()

  def disconnect(): Unit = {
    _disconnect.disconnect()
  }

  def checkConnection(): Unit = {
    _flowMonitor.state match {
      case Failed(cause) => throw new IllegalStateException("The modem stream has failed.", cause)
      case Finished => throw new IllegalStateException("The modem stream has terminated unexpectedly.")
      case _ => {}
    }
  }
}
