package notify.sinks

import call.Call
import play.api.libs.json.{JsString, JsValue, Json}
import uk.co.unclealex.push.BrowserPushService

import scala.concurrent.{ExecutionContext, Future}

/**
  * Send a call to each browser push service.
  * @param browserPushService The [[BrowserPushService]] that will send calls onwards.
  * @param ec
  */
class PushNotificationSink(
                      browserPushService: BrowserPushService)(implicit ec: ExecutionContext) extends CallSink {


  override def consumeCall(call: Call): Future[_] = {
    browserPushService.notifyAll(call.view)
  }

  override def consumeError(error: String): Future[_] = {
    val body: JsValue = Json.obj("error" -> JsString(error))
    browserPushService.notifyAll(body)
  }
}
