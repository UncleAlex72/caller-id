package notify.sinks

import call.{Call, CallView}
import uk.co.unclealex.squeezebox.SqueezeboxProgram._
import uk.co.unclealex.squeezebox.models.DisplayMessage
import uk.co.unclealex.squeezebox.{MediaServerClient, SqueezeboxProgram}

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ExecutionContext, Future}

/**
  * A sink that displays calls on all Squeezeboxes.
  * @param mediaServerClient The [[MediaServerClient]] used to send messages.
  */
class SqueezeboxSink(mediaServerClient: MediaServerClient, duration: FiniteDuration)(implicit ec: ExecutionContext) extends CallSink {
  override def consumeCall(call: Call): Future[_] = {
    val callView: CallView = call.view
    val message: String = (callView.contact, callView.phoneNumber) match {
      case (Some(contact), _) => s"${contact.name} (${contact.phoneType})"
      case (_, Some(phoneNumber)) =>
        val country: String = phoneNumber.countries.head match {
          case Left(name) => name
          case Right(countrySummary) => countrySummary.name
        }
        val address: String = (phoneNumber.city.toSeq :+ country).mkString(", ")
        s"${phoneNumber.formattedNumber} @ $address"
      case (_, _) => "Withheld"
    }
    val program: SqueezeboxProgram[Map[String, DisplayMessage]] =
      forEachPlayer(player => displayMessage(player.playerId, message, message, duration))
    mediaServerClient.execute(program)
  }

}
