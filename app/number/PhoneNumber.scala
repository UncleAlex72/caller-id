/**
 * Copyright 2013 Alex Jones
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 * @author alex
 *
 */
package number

import cats.data.NonEmptyList

/**
 * The representation of a telephone number that called.
 */
case class PhoneNumber(normalisedNumber: String,
                       formattedNumber: String,
                       city: Option[String],
                       countries: NonEmptyList[Either[String, CountrySummary]])

case class CountrySummary(name: String, isoCode: String)
object CountrySummary {
  def apply(country: Country): CountrySummary = {
    CountrySummary(country.name, country.isoCode)
  }
}

object PhoneNumber {
  import play.api.libs.json._
  import reactivemongo.bson._
  import codecs._

  private case class PhoneNumber_(
                                   normalisedNumber: String,
                                   formattedNumber: String,
                                   city: Option[String],
                                   countries: List[Either[String, CountrySummary]])

  implicit val countrySummaryFormat: OFormat[CountrySummary] = Json.format[CountrySummary]
  implicit val countrySummaryHandler: BSONDocumentHandler[CountrySummary] = Macros.handler[CountrySummary]

  implicit val stringOrCountrySummaryFormat: Format[Either[String, CountrySummary]] = {
    new Format[Either[String, CountrySummary]] {
      override def reads(json: JsValue): JsResult[Either[String, CountrySummary]] = {
        json match {
          case JsString(str) => JsSuccess(Left(str))
          case jsValue => countrySummaryFormat.reads(jsValue).map(Right(_))
        }
      }

      override def writes(o: Either[String, CountrySummary]): JsValue = {
        o match {
          case Left(name) => JsString(name)
          case Right(countrySummary) => countrySummaryFormat.writes(countrySummary)
        }
      }
    }
  }

  implicit val stringOrCountrySummaryFromStringHandler: BSONHandler[BSONValue, Either[String, CountrySummary]] = {
    new BSONHandler[BSONValue, Either[String, CountrySummary]] {
      override def write(t: Either[String, CountrySummary]): BSONValue = {
        t match {
          case Left(str) => BSONString(str)
          case Right(countrySummary) => countrySummaryHandler.write(countrySummary)
        }
      }

      override def read(bson: BSONValue): Either[String, CountrySummary] = {
        bson match {
          case BSONString(str) => Left(str)
          case doc : BSONDocument => Right(countrySummaryHandler.read(doc))
        }
      }
    }
  }

  implicit val phoneNumberFormat: OFormat[PhoneNumber] = Json.format[PhoneNumber]
  implicit val phoneNumberHandler: BSONDocumentHandler[PhoneNumber] = {
    val simpleHandler: BSONDocumentHandler[PhoneNumber_] = Macros.handler[PhoneNumber_]
    BSONDocumentHandler[PhoneNumber](
      read = bson => {
        val phoneNumber_ : PhoneNumber_ = simpleHandler.read(bson)
        PhoneNumber(
          phoneNumber_.normalisedNumber,
          phoneNumber_.formattedNumber,
          phoneNumber_.city,
          NonEmptyList.fromListUnsafe(phoneNumber_.countries))
      },
      write = phoneNumber => {
        val phoneNumber_ : PhoneNumber_ =
          PhoneNumber_(
            phoneNumber.normalisedNumber,
            phoneNumber.formattedNumber,
            phoneNumber.city,
            phoneNumber.countries.toList)
        simpleHandler.write(phoneNumber_)
      }
    )
  }
}