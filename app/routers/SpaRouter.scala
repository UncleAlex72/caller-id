package routers

import com.typesafe.scalalogging.StrictLogging
import controllers.Assets
import play.api.mvc.Handler
import play.api.routing.Router.Routes
import play.api.routing.SimpleRouter
import play.api.routing.sird._

class SpaRouter(val assets: Assets) extends SimpleRouter with StrictLogging {

  override def routes: Routes = {
    case GET(p"/") => handle("/index.html")
    case GET(p"/$file<[^.]+([.][^.]+)+>") => handle(file)
    case GET(p"/$route*") => handle("/index.html")
  }

  def handle(route: String): Handler = {
      assets.at(route)
  }
}
