import sbt.Keys._
import sbt._
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations._

name := """caller-id"""
organization := "uk.co.unclealex"

lazy val root = (project in file(".")).enablePlugins(PlayScala, DockerPlugin, AshScriptPlugin)

lazy val mongoDbVersion = "1.2.1"

scalaVersion := "2.13.0"

resolvers += Resolver.jcenterRepo

TwirlKeys.templateFormats += ("json" -> "uk.co.unclealex.templates.JsonFormat")

libraryDependencies ++= Seq(
  ws,
  ehcache,
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "com.beachape" %% "enumeratum" % "1.5.13",
  "uk.co.unclealex" %% "futures" % "1.0.1",
  "uk.co.unclealex" %% "akka-logitechmediaserver" % "1.0.3",
  "uk.co.unclealex" %% "play-browser-push-notifications" % "1.0.6",
  "uk.co.unclealex" %% "play-silhouette-mongo" % "1.1.1",
  "com.google.apis" % "google-api-services-people" % "v1-rev288-1.23.0"
)

libraryDependencies ++= Seq(
  "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.3",
  "uk.co.unclealex" %% "mongodb-scala-test" % mongoDbVersion
).map(_ % Test)

// Docker

dockerBaseImage := "unclealex72/docker-play-healthcheck:latest"
dockerExposedPorts := Seq(9000)
maintainer := "Alex Jones <alex.jones@unclealex.co.uk>"
dockerRepository := Some("unclealex72")
dockerUpdateLatest := true

javaOptions in Universal ++= Seq(
  "-Dpidfile.path=/dev/null"
)
// Releases

releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies, // : ReleaseStep
  inquireVersions, // : ReleaseStep
  runTest, // : ReleaseStep
  setReleaseVersion, // : ReleaseStep
  commitReleaseVersion, // : ReleaseStep, performs the initial git checks
  tagRelease, // : ReleaseStep
  releaseStepCommand("stage"), // : ReleaseStep, build server docker image.
  releaseStepCommand("docker:publish"), // : ReleaseStep, build server docker image.
  setNextVersion, // : ReleaseStep
  commitNextVersion, // : ReleaseStep
  pushChanges // : ReleaseStep, also checks that an upstream branch is properly configured
)
