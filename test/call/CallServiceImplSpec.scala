package call

import java.time.{Clock, Instant, ZoneId}

import cats.data._
import cats.implicits._
import contact.{Contact, ContactDao, PersistedContact, User}
import number.{City, Country, CountrySummary, PhoneNumber, PhoneNumberFactory}
import org.scalatest.{AsyncWordSpec, Matchers}

import scala.collection.SortedSet
import scala.concurrent.Future

class CallServiceImplSpec extends AsyncWordSpec with Matchers {

  val clock: Clock = Clock.fixed(Instant.parse("2018-05-20T15:39:00Z"), ZoneId.of("Europe/London"))
  val now: Instant = clock.instant()

  val London: City = City("London", "181")
  val UK: Country = Country("United Kingdom", "44", "GB", None, SortedSet(London))

  val knownPhoneNumber: PhoneNumber =
    PhoneNumber("+441818118181", "+44 (181) 8118181", Some("London"), NonEmptyList.one(Right(CountrySummary("United Kingdom", "UK"))))
  val unknownPhoneNumber: PhoneNumber =
    PhoneNumber("+441818228282", "+44 (181) 8118181", Some("London"), NonEmptyList.one(Right(CountrySummary("United Kingdom", "UK"))))
  val numberLocationService: PhoneNumberFactory = (number: String) =>
    Seq(knownPhoneNumber, unknownPhoneNumber).
      find(_.normalisedNumber == number).
      toValidNel("Don't know that phone number!")

  val BBC = Contact("+441818118181", "The BBC", "main", Some("http://bbc"))
  val persistedBBC = PersistedContact("user@here.now", BBC)

  val contactsService: ContactDao = new ContactDao {
    override def upsertUser(user: User): Future[Unit] = {
      throw new NotImplementedError("upsertUser")
    }
    override def findContactForPhoneNumber(normalisedPhoneNumber: String): Future[Option[PersistedContact]] = {
      Future.successful(Seq(persistedBBC).find(_.normalisedPhoneNumber == normalisedPhoneNumber))
    }

    override def findContactByName(name: String): Future[Option[PersistedContact]] = {
      Future.successful(Seq(persistedBBC).find(_.name == name))
    }
  }

  val callService = new CallServiceImpl(clock, numberLocationService, contactsService)

  def callOf(caller: Caller): Option[Either[String, Call]] = Some(Right(Call(None, now, caller)))

  "Ignorable responses" should {
    "be ignored" in {
      callService.call(modem.Ignore("me")).map { response =>
        response should ===(None)
      }
    }
  }

  "Unknown responses" should {
    "be flagged as an error" in {
      callService.call(modem.Unknown("unknown")).map { response =>
        response should ===(Some(Left("unknown")))
      }
    }
  }

  "WITHHELD responses" should {
    "show up as withheld" in {
      callService.call(modem.Withheld).map { response =>
        response should ===(callOf(Withheld))
      }
    }
  }

  "Contacts' telephone numbers" should {
    "identify the contact" in {
      callService.call(modem.Number("+441818118181")).map { response =>
        response should ===(callOf(Known("The BBC", "main", Some("http://bbc"), knownPhoneNumber)))
      }
    }
  }

  "Unknown telephone numbers" should {
    "echo the phone number" in {
      callService.call(modem.Number("+441818228282")).map { response =>
        response should ===(callOf(Unknown(unknownPhoneNumber)))
      }
    }
  }
}
