self.addEventListener('push', function(e) {
    const call = e.data.json();
    const contact = call.contact;
    const phoneNumber = call.phoneNumber;
    const error = call.error;
    const messageParts = [];

    if (error) {
        messageParts.push(`Error ${error}`)
    }
    else if (contact) {
        messageParts.push(`${contact.name}  (${contact.phoneType})`);
    }
    else if (phoneNumber) {
        messageParts.push(phoneNumber.formattedNumber);
        const address = phoneNumber.city ? phoneNumber.city + ", " : "";
        const countrySummary = phoneNumber.countries[0];
        const country = typeof(countrySummary) === "string" ? countrySummary : countrySummary.name;
        messageParts.push(address + country);
    }

    else if (!contact && !phoneNumber) {
        messageParts.push("Withheld call")
    }

    const body = messageParts.join("\n");

    const useAvatar = contact && contact.avatarUrl;
    const iconUrl = useAvatar ? contact.avatarUrl : "/caller-id/avatars/unknown.png";

    const actions = [];

    if (phoneNumber || contact) {
        actions.push({ action: "call-action", title: "Call"});
        if (!contact) {
            actions.push({ action: "search-action", title: "Search"});
            if (phoneNumber.city) {
                actions.push({ action: "map-action", title: "Map"})
            }
        }
    }

    const options = {
        body: body,
        icon: iconUrl,
        vibrate: [100, 50, 100],
        data: {
            dateOfArrival: Date.now(),
            primaryKey: '2',
            phoneNumber: phoneNumber,
            contact: contact
        },
        actions: actions
    };
    e.waitUntil(
        self.registration.showNotification('Landline call received', options)
    );
});

self.addEventListener('notificationclick', function(event) {
    const action = event.action;

    const urlBuilder = () => {
        const data = event.notification.data;
        switch (action || 'default') {
            case 'call-action':
                const number = data.phoneNumber ? data.phoneNumber.normalisedNumber : data.contact.normalisedPhoneNumber;
                return `tel:${number}`;
            case 'search-action':
                return `https://www.google.co.uk/?q=${encodeURIComponent(data.phoneNumber.normalisedNumber)}`;
            case 'map-action':
                return `https://maps.google.co.uk/?q=${encodeURIComponent(data.city + ", " + data.countries[0])}`;
            default:
                return '/caller-id/';
        }
    };
    const url = urlBuilder();
    const promiseChain = clients.openWindow(url);
    event.waitUntil(promiseChain)
});