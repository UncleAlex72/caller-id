export class Phase {
    name: string;

    constructor(name: string) {
        this.name = name;
    }
}

export const Phases = {
    INITIALISING: new Phase('INITIALISING'),
    READY: new Phase('READY'),
    UPDATING: new Phase('UPDATING')
};
