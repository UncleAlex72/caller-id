import {Phase, Phases} from './Phase'
import {AppState, Call, State, UserInfo} from "./models";
import {EMPTY, Observable, of, zip} from "rxjs";
import {List, Map} from "immutable";
import {catchError, flatMap, map, throwIfEmpty, withLatestFrom} from 'rxjs/operators';
import {combineEpics, Epic, StateObservable} from 'redux-observable';
import {parseCall, parseCalls, parseUserInfo} from "./parsers";
import {createSelector} from 'reselect'
import {concat} from "rxjs/internal/observable/concat";
import {maybe} from "./maybe";
import {filter} from "rxjs/internal/operators/filter";
import {
    actionExtractor,
    ErrorAwareEpicBuilder,
    errorIfNotValid,
    fetch$,
    fetchPushNotificationPublicKey$,
    installServiceWorker,
    listenFor,
    ReducerBuilder,
    requireAuthentication,
    storePushNotificationUrl,
    storePushNotificationUsername
} from "react-redux-observable-offline-support";
import {Action, Reducer} from "redux";
import {addToast} from "./toast";

export interface UpdatingAction extends Action<string> { type: "UPDATING", updating: boolean }
export interface UserInfoAction extends Action<string> { type: "USER_INFO", userInfo: UserInfo }
export interface HideCallAction extends Action<string> { type: "HIDE_CALL", call: Call, hide: boolean }
export interface UpdateLastHiddenCallAction extends Action<string> { type: "UPDATE_LAST_HIDDEN_CALL", id: string }
export interface ClearLastHiddenCallAction extends Action<string> { type: "CLEAR_LAST_HIDDEN_CALL" }
export interface CallUpdatedAction extends Action<string> { type: "CALL_UPDATED", call: Call }
export interface CallsAction extends Action<string> { type: "CALLS", calls: Map<string, Call> }
export interface ErroredAction extends Action<string> { type: "ERRORED", error: any }
export interface EnableAvatarsAction extends Action<string> { type: "ENABLE_AVATARS", enabled: boolean }
export interface ReloadAction extends Action<string> { type: "RELOAD" }
export interface UpdateContactsAction extends Action<string> { type: "UPDATE_CONTACTS" }
export interface InitialiseAction extends Action<string> { type: "INITIALISE" }
export interface InitialisedAction extends Action<string> { type: "INITIALISED" }

export type AppAction =
    UpdatingAction |
    UserInfoAction |
    CallsAction |
    CallUpdatedAction |
    HideCallAction |
    UpdateLastHiddenCallAction |
    ClearLastHiddenCallAction |
    ErroredAction |
    InitialiseAction |
    InitialisedAction |
    EnableAvatarsAction |
    ReloadAction |
    UpdateContactsAction

export function updating(updating: boolean): UpdatingAction {
    return {
        type: "UPDATING",
        updating
    };
}

export const isUpdating = actionExtractor<"UPDATING", UpdatingAction>("UPDATING");

export function userInfo(userInfo: UserInfo): UserInfoAction {
    return {
        type: "USER_INFO",
        userInfo
    }
}

export const isUserInfo = actionExtractor<"USER_INFO", UserInfoAction>("USER_INFO");

export function calls(calls: Map<string, Call>): CallsAction {
    return {
        type: "CALLS",
        calls
    }
}

export const isCalls = actionExtractor<"CALLS", CallsAction>("CALLS");

export function hideCall(call: Call, hide: boolean): HideCallAction {
    return {
        type: "HIDE_CALL",
        call,
        hide
    }
}

export const isHideCall = actionExtractor<"HIDE_CALL", HideCallAction>("HIDE_CALL");

export function callUpdated(call: Call): CallUpdatedAction {
    return {
        type: "CALL_UPDATED",
        call
    }
}

export const isCallUpdated = actionExtractor<"CALL_UPDATED", CallUpdatedAction>("CALL_UPDATED");

export function updateLastHiddenCall(id: string): UpdateLastHiddenCallAction {
    return {
        type: "UPDATE_LAST_HIDDEN_CALL",
        id
    }
}

export const isUpdateLastHiddenCall = actionExtractor<"UPDATE_LAST_HIDDEN_CALL", UpdateLastHiddenCallAction>("UPDATE_LAST_HIDDEN_CALL");

export function clearLastHiddenCall(): ClearLastHiddenCallAction {
    return {
        type: "CLEAR_LAST_HIDDEN_CALL"
    }
}

export const isClearLastHiddenCall = actionExtractor<"CLEAR_LAST_HIDDEN_CALL", ClearLastHiddenCallAction>("CLEAR_LAST_HIDDEN_CALL");

export function enableAvatars(enabled: boolean): EnableAvatarsAction {
    return {
        type: "ENABLE_AVATARS",
        enabled
    }
}

export const isEnableAvatars = actionExtractor<"ENABLE_AVATARS", EnableAvatarsAction>("ENABLE_AVATARS");

export function errored(error: any): ErroredAction {
    return {
        type: "ERRORED",
        error
    }
}

export const isErrored = actionExtractor<"ERRORED", ErroredAction>("ERRORED");

export function updateContacts(): UpdateContactsAction {
    return {
        type: "UPDATE_CONTACTS"
    }
}

export const isUpdateContacts = actionExtractor<"UPDATE_CONTACTS", UpdateContactsAction>("UPDATE_CONTACTS");

export function reload(): ReloadAction {
    return {
        type: "RELOAD"
    }
}

export const isReload = actionExtractor<"RELOAD", ReloadAction>("RELOAD");

export function initialise(): InitialiseAction {
    return {
        type: "INITIALISE"
    }
}

export const isInitialise = actionExtractor<"INITIALISE", InitialiseAction>("INITIALISE");

export function initialised(): InitialisedAction {
    return {
        type: "INITIALISED"
    }
}

export const isInitialised = actionExtractor<"INITIALISED", InitialisedAction>("INITIALISED");


export const initialState: AppState = { phase: Phases.INITIALISING, enableAvatars: true };

export const app: Reducer<AppState, AppAction> = new ReducerBuilder(initialState)
    .when(isInitialise, () => state => {
        return {
            ...state,
            phase: Phases.INITIALISING
        };
    })
    .when(isErrored, action => state => {
        const error = action.error;
        return {
            ...state,
            error
        };
    })
    .when(isCalls, action => state => {
        const calls = action.calls;
        return {
            ...state,
            calls
        };
    })
    .when(isCallUpdated, action => state => {
        const updatedCall = action.call;
        const calls = (state.calls || Map()).set(updatedCall.id, action.call);
        return {
            ...state,
            calls
        };
    })
    .when(isUserInfo, action => state => {
        const userInfo = action.userInfo;
        return {
            ...state,
            userInfo
        };
    })
    .when(isUpdateLastHiddenCall, action => state => {
        const lastHiddenCallId = action.id;
        return {
            ...state,
            lastHiddenCallId
        };
    })
    .when(isClearLastHiddenCall, () => state => {
        return {
            ...state,
            lastHiddenCallId: undefined
        };
    })
    .when(isEnableAvatars, action => state => {
        const enableAvatars = action.enabled;
        return {
            ...state,
            enableAvatars
        };
    })
    .when(isUpdating, action => state => {
        const phase = action.updating ? Phases.UPDATING : Phases.READY;
        return {
            ...state,
            phase
        };
    })
    .build();

function required<V>(value: V | undefined, msg: string): V {
    if (value === undefined) {
        throw new Error(msg)
    }
    return value;
}

const getState: (state: State) => AppState = state => state.app;

function getFromState<T>(extractor: (state: AppState) => T): (state: State) => T {
    return createSelector(getState, extractor)
}

export const getPhase: (state: State) => Phase = getFromState(state => state.phase);

export const getError: (state: State) => any = getFromState(state => state.error);

export const getUserInfo: (state: State) => UserInfo  = getFromState(state =>
    required(state.userInfo, "userInfo is not defined"));

export const areAvatarsEnabled: (state: State) => boolean  = getFromState(state => state.enableAvatars);

export const getCalls: (state: State) => Map<string, Call> = createSelector(
    getState,
    state => required(state.calls, "calls is not defined")
);

export const getCallsById: (state: State) => Map<string, Call> = createSelector(
    getCalls,
    calls => calls.filter(call => !call.hidden)
);

export const getCallIds: (state: State) => List<string> = createSelector(
    getCallsById,
    (callsById) => {
        function comparator(id: string): number {
            const maybeCall: Call | undefined = callsById.get(id);
            const seconds = maybe(maybeCall, call => -call.when.unix());
            return seconds || 0;
        }
        return callsById.entrySeq()
            .filter(kv => !kv[1].hidden)
            .map(kv => kv[0])
            .toList()
            .sortBy(comparator)
    }
);

export const getAvatarsById: (state: State) => Map<string, string> = createSelector(
    areAvatarsEnabled, getCallsById,
    (enabled, callsById) => {
        if (enabled) {
            return callsById.flatMap((call, id) => {
                if (call.contact) {
                    return List.of<[string, string]>([id, url(`/api/avatar/${call.contact.name}`)])
                }
                else {
                    return List.of<[string, string]>();
                }
            });
        }
        else {
            return Map();
        }
    }
);

function url(path: string): string {
    return "/caller-id" + path;
}

function loadDataEpicBuilder<T extends string>(isAction: (a: Action<T>) => Action<T> | undefined, actions$: Observable<Action<string>>): Epic {
    return action$ => action$.pipe(
        listenFor(isInitialise),
        flatMap(() => {
            const userInfo$: Observable<UserInfo> = fetch$(url("/api/userinfo")).pipe(
                errorIfNotValid(),
                flatMap(response => response.json$<object[]>()),
                map(parseUserInfo)
            );
            const calls$: Observable<Map<string, Call>> = fetch$(url("/api/calls")).pipe(
                errorIfNotValid(),
                flatMap(response => response.json$<object[]>()),
                map(parseCalls)
            );
            const userInfoAndCalls$ = zip(userInfo$, calls$);
            const result = userInfoAndCalls$.pipe(
                flatMap(([_userInfo, _calls]) => {
                    return concat(
                        of(userInfo(_userInfo), calls(_calls), enableAvatars(false), enableAvatars(true)))
                }),
                throwIfEmpty(() => "no response from initialisation"));
            return concat(of(updating(true)), result, actions$);
        })
    );
}

const initialiseEpic = loadDataEpicBuilder(isInitialise, of(updating(false), initialised()));
const reloadEpic = loadDataEpicBuilder(isReload, of(updating(false)));

const updateCallEpic: Epic = (action$) => action$.pipe(
    listenFor(isHideCall),
    flatMap((hideCallAction: HideCallAction) => {
            const {call, hide} = hideCallAction;
            return fetch$(
                url(`/api/calls/${call.id}/hidden`),
                {
                    method: "PUT",
                    body: JSON.stringify(hide),
                    headers: new Headers({ "content-type": "application/json" })
                }).pipe(
                    errorIfNotValid(),
                    flatMap(response => response.json$<object>()),
                    map(parseCall),
                    flatMap(call => {
                        const updateHiddenAction = call.hidden ?
                            updateLastHiddenCall(call.id) : clearLastHiddenCall();
                        return of(callUpdated(call), updateHiddenAction);
                    })
            )
        }
    ),
    catchError(error => of(errored(error)))
);

const updateContactsEpic: Epic = action$ => action$.pipe(
    listenFor(isUpdateContacts),
    flatMap(() =>
        fetch$(url("/api/contacts"), { method : "POST" }).pipe(
            errorIfNotValid(),
            flatMap(() => of(reload()))
        )
    )
);

const showToastOnHideCallEpic: Epic = action$ => action$.pipe(
    listenFor(isHideCall),
    filter(hideCallAction => (<HideCallAction>hideCallAction).hide),
    map(hideCallAction => {
        const call = (<HideCallAction>hideCallAction).call;
        return addToast({call})
    })
);

const installServiceWorkerOnceInitialisedEpic: Epic = (action$, state$: StateObservable<State>) => action$.pipe(
    listenFor(isInitialised),
    withLatestFrom(state$),
    flatMap(([{}, state]: ([InitialisedAction, State])) => {
        const username = getUserInfo(state).fullName;
        if (username) {
            const username$ = of(storePushNotificationUsername(username));
            const publicKey$ = fetchPushNotificationPublicKey$(url("/push/key"));
            const storePushNotificationUrl$ = of(storePushNotificationUrl(url("/push/subscribe")));
            const installServiceWorker$ = of(installServiceWorker());
            return concat(username$, publicKey$, storePushNotificationUrl$, installServiceWorker$)
        }
        else {
            return EMPTY;
        }
    })
);

const errorAware = new ErrorAwareEpicBuilder()
    .onAuthenticationRequired(() => of(requireAuthentication()))
    .onError((err: any) => of(errored(err), updating(false)))
    .build();

export const rootEpic = errorAware(
    combineEpics(
        initialiseEpic,
        reloadEpic,
        showToastOnHideCallEpic,
        updateContactsEpic,
        updateCallEpic,
        installServiceWorkerOnceInitialisedEpic
    )
);