import * as React from 'react';
import {Phase, Phases} from "../Phase";
import {connect} from 'react-redux'
import {getError, getPhase} from "../app";
import {State} from "../models";
import Shell from "./Shell";
import Error from "./Error"
import {DisplayInitialiserProps, isAuthenticationRequired} from "react-redux-observable-offline-support";
import Authenticate from "./Authenticate";

export interface StateProps {
    readonly phase: Phase,
    readonly showAuth: boolean
    readonly showError: boolean
}

export type Props = StateProps & DisplayInitialiserProps
const App = ({phase, showError, showAuth, onDisplayInitialised}: Props) => {
    if (phase === Phases.INITIALISING) {
        return <div/>
    }
    else {
        onDisplayInitialised();
        if (showAuth) {
            return (<Authenticate/>)
        }
        else {
            switch (phase) {
                case Phases.INITIALISING:
                    return (<div/>);
                case Phases.UPDATING:
                    return (<div/>);
                default:
                    if (showError) {
                        return <Error/>
                    } else {
                        return <Shell/>
                    }
            }
        }
    }
};

function mapStateToProps(state: State): StateProps {
    const phase = getPhase(state);
    const showError = !!getError(state);
    const showAuth = isAuthenticationRequired(state);
    return {phase, showAuth, showError};
}

function mergeProps(stateProps: StateProps, _undefined: undefined, displayInitialiserProps: DisplayInitialiserProps): Props {
    return {
        ...stateProps,
        ...displayInitialiserProps
    }
}

export default connect(
    mapStateToProps,
    undefined,
    mergeProps
)(App);