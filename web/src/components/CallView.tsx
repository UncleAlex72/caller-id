import {Call, CountrySummary, PhoneNumber, State} from "../models";
import {Avatar, Button, Card, CardActions, CardTitle} from "react-md";
import * as React from "react";
import {ReactElement} from "react";
import ring from "../ring.svg"
import {List} from "immutable";
import {maybeList} from "../maybe";
import {connect} from 'react-redux'
import {getAvatarsById, getCallsById, hideCall} from "../app"
import {Dispatch} from "redux";
import {isOffline} from "react-redux-observable-offline-support";
import {objectOf} from "prop-types";

export interface OwnProps {
    readonly id: string
}

export interface OfflineProps {
    readonly offline: boolean
}

export interface StateProps {
    readonly call: Call,
    readonly avatar?: string,
}

export interface DispatchProps {
    readonly removeCall?: (Call: Call) => () => void
}

export type Props = StateProps & DispatchProps & OwnProps & OfflineProps

function toAddress(phoneNumber: PhoneNumber, long: boolean): string {
    const city: List<string> = phoneNumber.city ? List.of(phoneNumber.city) : List.of();
    const country: List<CountrySummary | string> = phoneNumber.countries.take(1);
    const formattedCountry: List<String> = country.map(nameOrSummary => {
            if (typeof (nameOrSummary) === "string") {
                return nameOrSummary;
            } else {
                return long ? nameOrSummary.name : nameOrSummary.isoCode.toUpperCase();
            }
        }
    );
    return city.concat(formattedCountry).join(", ");
}

const CallView = ({call, avatar, removeCall}: Props) => {
    const avatarSource = avatar || ring;
    const longTitle = call.when.format("dddd, MMMM Do YYYY, h:mm a");
    const shortTitle = call.when.format("DD/MM/YY h:mm a");
    function subtitle(long: boolean): string {
        if (call.contact) {
            return call.contact.name;
        }
        else if (call.phoneNumber) {
            const phoneNumber = call.phoneNumber;
            return `${phoneNumber.formattedNumber} (${toAddress(phoneNumber, long)})`;
        }
        else {
            return "Withheld";
        }
    }
    const actionButtons: List<ReactElement> = (() => {
       const maybeCallButton: List<ReactElement> = maybeList(call.phoneNumber, phoneNumber =>
           <Button key={"call"} href={`tel:${phoneNumber.normalisedNumber}`} target={"_blank"} icon>call</Button>
       );
       const maybeSearchButton: List<ReactElement> = maybeList(call.phoneNumber, phoneNumber =>
           <Button key={"search"} href={`https://www.google.co.uk/?q=${phoneNumber.normalisedNumber}`} target={"_blank"} icon>search</Button>
       );
       const maybeMapsButton: List<ReactElement> = maybeList(call.phoneNumber, phoneNumber =>
           <Button key={"maps"} href={`https://maps.google.co.uk/?q=${toAddress(phoneNumber, true)}`} target={"_blank"} icon>my_location</Button>
       );
       const unknownNumberButtons = (maybeSearchButton.concat(maybeMapsButton)).filter(() => call.contact === undefined);
       const disabled = !removeCall;
       const onClick = removeCall || (() => () => {});
       const removeButton: ReactElement = <Button key={"remove"} icon disabled={disabled} onClick={onClick(call)} secondary>delete</Button>;
        return maybeCallButton.concat(unknownNumberButtons).push(removeButton);
    })();

    return (
        <div className={"call"}>
            <div className={"anchor"} id={call.id}>&nbsp;</div>
            <Card className={"call-card"}>
                <CardTitle
                    className={"show-on-desktop"}
                    title={longTitle}
                    subtitle={subtitle(true)}
                    avatar={<Avatar src={avatarSource} role="presentation" />}
                />
                <CardTitle
                    className={"hide-on-desktop"}
                    title={shortTitle}
                    subtitle={subtitle(false)}
                    avatar={<Avatar src={avatarSource} role="presentation" />}
                />
                <CardActions centered={true}>
                    {actionButtons}
                </CardActions>
            </Card>
        </div>
    );
};

function mapStateToProps(state: State, {id}: OwnProps): StateProps & OfflineProps {
    const call = getCallsById(state).get(id);
    if (call) {
        const avatar = getAvatarsById(state).get(id);
        const offline = isOffline(state);
        return {call, avatar, offline };
    }
    else {
        throw new Error(`Cannot find a call with id ${id}`)
    }
}

function mapDispatchToProps(dispatch: Dispatch): DispatchProps {
    const removeCall : (call: Call) => () => void = call => function() {
        dispatch(hideCall(call, true));
    };
    return { removeCall }
}

function mergeProps(
    stateAndOfflineProps: StateProps & OfflineProps,
    dispatchProps: DispatchProps,
    ownProps: OwnProps): Props {
    const offline = stateAndOfflineProps.offline;
    if (offline) {
        return { ...stateAndOfflineProps, ...ownProps };
    }
    else {
        return { ...stateAndOfflineProps, ...dispatchProps, ...ownProps };
    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps,
    mergeProps
)(CallView);