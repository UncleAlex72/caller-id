import * as React from 'react';
import {connect} from 'react-redux'
import {getError} from "../app";
import {Props} from "../components/Error";
import {State} from "../models";

export interface Props {
    readonly error?: any
}

const Error = (props: Props) => {
    return <div>
        Error "{props.error}" occurred
    </div>
};

function mapStateToProps(state: State): Props {
    const error = getError(state);
    return {error};
}

export default connect(
    mapStateToProps
)(Error);