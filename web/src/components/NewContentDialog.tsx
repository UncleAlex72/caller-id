import * as React from 'react';
import {Button, DialogContainer} from "react-md";
import {connect} from 'react-redux'
import {State} from "../models";
import {Dispatch} from "redux";
import {isNewContentAlertVisible} from "react-redux-observable-offline-support";

export interface StateProps {
    readonly visible: boolean
}

export interface DispatchProps {
    readonly onHide: () => void
}

export type Props = StateProps & DispatchProps

const NotificationDialog = ({visible, onHide}: Props) => {
    const actions = [
        <Button flat primary onClick={onHide}>Okay</Button>
    ];

    return (
        <DialogContainer
            id="refresh-dialog"
            visible={visible}
            onHide={onHide}
            actions={actions}
            title="New Content Available">
            <p>New content is available. Please reload this page.</p>
        </DialogContainer>
    )
};

const mapStateToProps: (s: State) => StateProps = state => {
    const visible = isNewContentAlertVisible(state);
    return { visible };
};

const mapDispatchToProps: (d: Dispatch) => DispatchProps = () => {
    return {
        onHide: () => {
            window.location.reload();
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NotificationDialog);
