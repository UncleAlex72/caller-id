import * as React from 'react';
import {Button, DialogContainer} from "react-md";
import {connect} from 'react-redux'
import {State} from "../models";
import {Dispatch} from "redux";
import {isNotificationAlertVisible, dismissRequestPushNotificationsAlert, subscribeToPushNotifications} from "react-redux-observable-offline-support";


export interface StateProps {
    readonly visible: boolean
}

export interface DispatchProps {
    readonly onHide: () => void
    readonly onAcceptPushNotifications: () => void
}

export type Props = StateProps & DispatchProps

const NotificationDialog = ({visible, onHide, onAcceptPushNotifications}: Props) => {
    const actions = [
        <Button flat primary onClick={onAcceptPushNotifications}>Yes</Button>,
        { secondary: true, children: 'No', onClick: onHide }
    ];

    return (
        <DialogContainer
            id="notifications-dialog"
            visible={visible}
            onHide={onHide}
            actions={actions}
            title="Subscribe to push notifications?">
            <p>Subscribe to push notifications to be kept up to date with any changes.</p>
        </DialogContainer>
    )
};

const mapStateToProps: (s: State) => StateProps = state => {
    const visible = isNotificationAlertVisible(state);
    return { visible };
};

const mapDispatchToProps: (d: Dispatch) => DispatchProps = dispatch => {
    return {
        onHide: () => {
            dispatch(dismissRequestPushNotificationsAlert());
        },
        onAcceptPushNotifications: () => {
            [dismissRequestPushNotificationsAlert(), subscribeToPushNotifications()].forEach(dispatch)
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NotificationDialog);