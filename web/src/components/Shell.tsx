import * as React from 'react';
import {State, UserInfo} from "../models";
import {List} from "immutable";
import {Cell, FontIcon, Grid, ListItem, NavigationDrawer} from "react-md";
import NewContentDialog from "./NewContentDialog";
import NotificationsDialog from "./NotificationsDialog";
import {OfflineSupportDetection} from "react-redux-observable-offline-support";
import {connect} from 'react-redux'
import {DispatchProps, StateProps} from "../components/Shell";
import {getCallIds, getUserInfo, updateContacts} from "../app";
import {Dispatch} from "redux";
import CallView from "./CallView";
import ToastRack from "./ToastRack";

export interface StateProps {
    readonly userInfo: UserInfo,
    readonly callIds: List<string>
}

export interface DispatchProps {
    readonly onUpdateContacts: () => void
}
export type Props = DispatchProps & StateProps

interface NavItemObj {
    readonly key: string,
    readonly text: string,
    readonly icon: string,
    readonly action?: ()=> void}

const Shell = ({userInfo, callIds, onUpdateContacts}: Props) => {
    const navItemObjs: NavItemObj[] = [
        {key: "update", text: "Update Contacts", icon: "home", action: onUpdateContacts}
        ];
    const navItems = navItemObjs.map(ni => {
            const icon = <FontIcon>{ni.icon}</FontIcon>;
            return <ListItem key={ni.key} primaryText={ni.text} onClick={ni.action} leftIcon={icon}/>
        });
    const name = userInfo.firstName || userInfo.fullName || "Somebody";
    return (
        <NavigationDrawer
            toolbarTitle={name}
            navItems={navItems}
            mobileDrawerType={NavigationDrawer.DrawerTypes.TEMPORARY}
            tabletDrawerType={NavigationDrawer.DrawerTypes.TEMPORARY}
            desktopDrawerType={NavigationDrawer.DrawerTypes.TEMPORARY}>
            <Grid>
                {callIds.map(id =>
                        <Cell key={id} size={12} tabletSize={12} phoneSize={12}>
                            <CallView id={id}/>
                        </Cell>
                    )}
            </Grid>
            <ToastRack/>
            <NewContentDialog/>
            <NotificationsDialog/>
            <OfflineSupportDetection/>
        </NavigationDrawer>
    );
};

function mapStateToProps(state: State): StateProps {
    const userInfo = getUserInfo(state);
    const callIds = getCallIds(state);
    return {userInfo, callIds};
}

function mapDispatchToProps(dispatch: Dispatch): DispatchProps {
    const onUpdateContacts = () => {
        dispatch(updateContacts())
    };
    return {onUpdateContacts}
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Shell);