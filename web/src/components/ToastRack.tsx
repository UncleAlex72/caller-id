import * as React from 'react';
import {Call, State, Toast} from "../models";
import {List} from "immutable";
import {ButtonProps, Snackbar} from "react-md";
import {connect} from 'react-redux'
import {Dispatch} from "redux";
import {dismissToast, getToasts} from "../toast";
import {hideCall} from "../app";

export interface StateProps {
    readonly toasts: List<Toast>
}

export interface DispatchProps {
    readonly onDismiss: () => void,
    readonly onRestoreCall: (call: Call) => () => void
}
export type Props = DispatchProps & StateProps

const ToastRackComponent = ({toasts, onDismiss, onRestoreCall}: Props) => {
    const realToasts: List<{ action: ButtonProps; text: string }> = toasts.map(toast => {
        const call = toast.call;
        const text = `Removed call at ${call.when.format("DD/MM/YY h:mm a")}`;
        const action: ButtonProps = {
            children: <span>Undo</span>,
            onClick: onRestoreCall(call)
        };
        return { text, action }
    });
    return (
        <Snackbar
            id="toast-rack"
            toasts={realToasts.toArray()}
            autohide={true}
            autohideTimeout={6000}
            autoFocusAction
            onDismiss={onDismiss}
        />
    );
};

function mapStateToProps(state: State): StateProps {
    const toasts = getToasts(state);
    return {toasts};
}

function mapDispatchToProps(dispatch: Dispatch): DispatchProps {
    const onDismiss = () => {
        dispatch(dismissToast())
    };
    const onRestoreCall = (call: Call) => () => {
        dispatch(hideCall(call, false))
    };
    return {onDismiss, onRestoreCall}
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ToastRackComponent);