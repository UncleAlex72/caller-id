import React from 'react';
import './index.scss';
import {applyMiddleware, combineReducers, createStore} from "redux";
import {composeWithDevTools} from "redux-devtools-extension";
import {app, initialise, rootEpic} from "./app";
import {toast} from "./toast";
import {Provider} from "react-redux";
import {combineEpics, createEpicMiddleware} from 'redux-observable';
import {
    authenticationRequired,
    initialiseDisplay,
    offline,
    serviceWorker,
    serviceWorkerAlerts,
    serviceWorkerWithPushNotificationsEpic
} from "react-redux-observable-offline-support";
import App from "./components/App";

const epicMiddleware = createEpicMiddleware();

const store = createStore(
    combineReducers({app, toast, serviceWorker, serviceWorkerAlerts, offline, authenticationRequired}),
    {},
    composeWithDevTools(
        applyMiddleware(
            epicMiddleware
        )
    )
);

const epicBuilder = (root: HTMLElement) => combineEpics(rootEpic, serviceWorkerWithPushNotificationsEpic(root));
initialiseDisplay(
    store,
    epicMiddleware,
    "root",
    "loading",
    ipd => (
        <Provider store={store}>
            <App onDisplayInitialised={ipd}/>
        </Provider>),
    epicBuilder,
    initialise()
);