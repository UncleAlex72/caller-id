import {List} from "immutable";

export function maybe<T, U>(t: T | undefined, mapper: (t: T) => U): U | undefined {
    if (t === undefined) {
        return undefined;
    }
    else {
        return mapper(t);
    }
}

export function maybeList<T, U>(t: T | undefined, mapper: (t: T) => U): List<U> {
    const maybeElement = maybe(t, mapper);
    return maybeElement ? List.of(maybeElement) : List.of();
}