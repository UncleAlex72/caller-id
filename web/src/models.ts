import {Moment} from "moment";
import {Phase} from "./Phase";
import {Map, List} from "immutable";
import {
    PushNotificationState,
    OfflineState,
    ServiceWorkerAlertsState,
    AuthenticationRequiredState
} from "react-redux-observable-offline-support"

export type UserInfo = {
    readonly firstName?: string,
    readonly lastName?: string,
    readonly fullName?: string,
    readonly email?: string
}

export type Contact = {
    readonly normalisedPhoneNumber: string,
    readonly name: string,
    readonly phoneType: string
}

export type PhoneNumber = {
    readonly normalisedNumber: string,
    readonly formattedNumber: string,
    readonly city?: string,
    readonly countries: List<CountrySummary | string>
}

export type CountrySummary = {
    readonly name: string,
    readonly isoCode: string
}

export type Call = {
    readonly id: string,
    readonly when: Moment,
    readonly contact?: Contact,
    readonly phoneNumber?: PhoneNumber,
    readonly hidden: boolean
}

export type AppState = {
    readonly phase: Phase,
    readonly userInfo?: UserInfo,
    readonly calls?: Map<string, Call>,
    readonly enableAvatars: boolean,
    readonly lastHiddenCallId?: string,
    readonly error?: any
}

export type Toast = {
    readonly call: Call,
}

export type ToastState = {
    readonly toasts: List<Toast>
}

export type State = {
    readonly app: AppState,
    readonly toast: ToastState,
    readonly serviceWorker: PushNotificationState,
    readonly offline: OfflineState
    readonly serviceWorkerAlerts: ServiceWorkerAlertsState,
    readonly authenticationRequired: AuthenticationRequiredState

}
