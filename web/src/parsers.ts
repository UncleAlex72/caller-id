import {Call, Contact, PhoneNumber, UserInfo} from "./models";
import {List, Map} from "immutable";
import moment, {Moment} from "moment";

function maybeParse<T>(fn: (o: object) => T): (o : object | undefined) => T | undefined {
    return function(o: object | undefined) {
      if (o === undefined) {
          return undefined;
      }
      else {
          return fn(o);
      }
    };
}
function parseContact(obj: object): Contact {
    // @ts-ignore
    const { normalisedPhoneNumber, name, phoneType } = obj;
    return {
        normalisedPhoneNumber,
        name,
        phoneType
    }
}

function parsePhoneNumber(obj: object): PhoneNumber {
    // @ts-ignore
    const {normalisedNumber, formattedNumber, city, countries} = obj;
    return {
        normalisedNumber,
        formattedNumber,
        city,
        countries: List(countries)
    }

}

function parseWhen(when: string): Moment {
    return moment(when)
}

export function parseCall(obj: object): Call {
    // @ts-ignore
    const {id, hidden, when, phoneNumber, contact} = obj;
    return {
        id,
        when: parseWhen(when),
        hidden,
        phoneNumber: maybeParse(parsePhoneNumber)(phoneNumber),
        contact: maybeParse(parseContact)(contact)
    }
}

export function parseCalls(objs: object[]): Map<string, Call> {
    function reducer(map: Map<string, Call>, obj: object): Map<string, Call> {
        const call = parseCall(obj);
        return map.set(call.id, call)
    }
    const empty: Map<string, Call> = Map();
    return objs.reduce(reducer, empty);
}

export function parseUserInfo(obj: object): UserInfo {
    // @ts-ignore
    const {firstName, lastName, fullName, email} = obj;
    return {
        firstName,
        lastName,
        fullName,
        email
    }
}