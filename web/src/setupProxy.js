const proxy = require('http-proxy-middleware');

module.exports = function(app) {
    ["api", "authenticate", "push", "webhook"].forEach(path => {
        app.use(proxy(`/caller-id/${path}`, {target: 'http://localhost:9000', xfwd: true}));
    });
};