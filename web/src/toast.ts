import {State, Toast, ToastState} from "./models";
import {List} from "immutable";
import {createSelector} from 'reselect'
import {actionExtractor, ReducerBuilder} from "react-redux-observable-offline-support";
import {Action, Reducer} from "redux";

export interface AddToastAction extends Action<string> { type: "ADD_TOAST", toast: Toast }
export interface DismissToastAction extends Action<string> { type: "DISMISS_TOAST" }

export type ToastAction = AddToastAction | DismissToastAction

export function addToast(toast: Toast): AddToastAction {
    return {
        type: "ADD_TOAST",
        toast
    }
}

export const isAddToast = actionExtractor<"ADD_TOAST", AddToastAction>("ADD_TOAST");

export function dismissToast(): DismissToastAction {
    return {
        type: "DISMISS_TOAST"
    }
}

export const isDismissToast = actionExtractor<"DISMISS_TOAST", DismissToastAction>("DISMISS_TOAST");

export const initialState: ToastState = { toasts: List.of() };

export const toast: Reducer<ToastState, ToastAction> = new ReducerBuilder(initialState)
    .when(isAddToast, action => state => {
        const toasts = state.toasts.push(action.toast);
        return {
            ...state,
            toasts
        }
    })
    .when(isDismissToast, () => state => {
        const toasts = state.toasts.delete(0);
        return {
            ...state,
            toasts
        }
    })
    .build();

const getState: (state: State) => ToastState = state => state.toast;

function getFromState<T>(extractor: (state: ToastState) => T): (state: State) => T {
    return createSelector(getState, extractor)
}

export const getToasts: (state: State) => List<Toast> = getFromState(state => state.toasts);
